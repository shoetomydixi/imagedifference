package com.core.lib;

import java.awt.image.BufferedImage;

/**
 * Created by deniskashuba on 10.11.16.
 */
public class ImageHelper {

    public int getRgb(BufferedImage image, int j, int i) {

        return image.getRGB(j, i);

    }

    public int getR(int rgb) {

        return (rgb >> 16) & 0xff;

    }

    public int getG(int rgb) {

        return (rgb >> 8) & 0xff;

    }

    public int getB(int rgb) {

        return (rgb) & 0xff;

    }

    public int getA(int rgb) {

        return (rgb & 0xff000000) >>> 24;

    }

}
