/**
 * Created by deniskashuba on 10.11.16.
 */
package com.core.lib;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 *
 *
 *
 *
 */
public class ImageReader {

    /**
     *
     * @param pathToImage
     * @return
     * @throws IOException
     */
    public BufferedImage ReadImage(String pathToImage) throws IOException {

        return ImageIO.read(new File(pathToImage));

    }

    /**
     *
     * @param pathToSave
     * @param imageToSave
     * @throws IOException
     */
    public void SaveImage(String pathToSave, BufferedImage imageToSave) throws IOException {

        File outputfile = new File(pathToSave);
        ImageIO.write(imageToSave, "png", outputfile);

    }

}
