package com.core.lib;

import java.awt.image.BufferedImage;

/**
 * Created by deniskashuba on 10.11.16.
 */
public class ImageProcessor {

    /**
     *
     * @param image1
     * @param image2
     * @param percentsDiff
     * @param excludeX
     * @param excludeY
     * @param exclideWidth
     * @param excludeHeight
     * @return
     */
    public BufferedImage CompareImages(BufferedImage image1, BufferedImage image2, int percentsDiff, int excludeX, int excludeY, int excludeWidth, int excludeHeight) {

        int diff;
        int originalPixel;

        ImageHelper imageHelper = new ImageHelper();

        int width1 = image1.getWidth();
        int width2 = image2.getWidth();

        int height1 = image1.getHeight();
        int height2 = image2.getHeight();

        BufferedImage outputImg = new BufferedImage(width1, height1, BufferedImage.TYPE_INT_RGB);

        if ((width1 != width2) || (height1 != height2)) {
            System.err.println("Error: Images dimensions mismatch");
            System.exit(1);
        }

        int diffValue = Math.round(percentsDiff * 255 / 100);

        for (int i = 0; i < height1; i++) {

            for (int j = 0; j < width1; j++) {

                originalPixel = 0;
                diff = 0;

                int rgb1 = imageHelper.getRgb(image1, j, i);
                int rgb2 = imageHelper.getRgb(image2, j, i);

                int r1 = imageHelper.getR(rgb1);
                int g1 = imageHelper.getG(rgb1);
                int b1 = imageHelper.getB(rgb1);
                int a1 = imageHelper.getA(rgb1);

                int r2 = imageHelper.getR(rgb2);
                int g2 = imageHelper.getG(rgb2);
                int b2 = imageHelper.getB(rgb2);
                int a2 = imageHelper.getA(rgb2);

                diff = Math.abs(r1 - r2);
                diff += Math.abs(g1 - g2);
                diff += Math.abs(b1 - b2);
                diff += Math.abs(a1 - a2);
                diff /= 3;

                originalPixel = (r2 << 16) | (g2 << 8) | b2;

                if (i >= excludeY &&
                        j >= excludeX &&
                        i <= (excludeY + excludeHeight) &&
                        j <= (excludeX + excludeWidth)) {

                    fillImage(outputImg, i, j, originalPixel);
                    continue;

                }

                if (diff > diffValue) {

                    originalPixel = (0 << 24) | (0 << 16) | (205 << 8) | 255;

                    fillImage(outputImg, i, j, originalPixel);

                }else {

                    fillImage(outputImg, i, j, originalPixel);

                }

            }

        }

        return outputImg;

    }

    private void fillImage(BufferedImage outputImg, int i, int j, int pixel) {

        outputImg.setRGB(j, i, pixel);

    }

}
