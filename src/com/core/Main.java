package com.core;

import com.core.lib.ImageProcessor;
import com.core.lib.ImageReader;

import java.awt.image.BufferedImage;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        try {

            ImageReader imageReader = new ImageReader();
            ImageProcessor imageProcessor = new ImageProcessor();

            BufferedImage img1 = imageReader.ReadImage(System.getProperty("user.dir") + "/assets/test_3.jpg");
            BufferedImage img2 = imageReader.ReadImage(System.getProperty("user.dir") + "/assets/test_4.jpg");

            BufferedImage outImg = imageProcessor.CompareImages(img1, img2, 15, 110, 200, 100, 100);

            imageReader.SaveImage(System.getProperty("user.dir") + "/assets/save_1.png", outImg);

        } catch (IOException e) {

            System.err.println("Caught IOException: " + e.getMessage());

        }

    }

}
